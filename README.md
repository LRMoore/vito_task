# Task

## installation instructions

### Install (Ana/mini)conda (recommended)

[Anaconda](https://www.anaconda.com/distribution/ "Anaconda Homepage") as of 2018 can conveniently
 bootstrap a working tensorflow-gpu environment, so is recommended!

### set up anaconda environment (tensorflow-gpu + python 3.7)

```
conda env create -f conda_env.yml
```

## usage

Demo code is in the jupyter notebook ('task.ipynb')
