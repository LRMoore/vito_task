import os

import numpy as np
import pandas as pd
import sklearn
import cv2
import matplotlib.pyplot as plt

from functools import partial

from mpl_toolkits import axes_grid1
from scipy import ndimage
from skimage.color import rgb2gray
from sklearn.cluster import DBSCAN
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import FunctionTransformer, minmax_scale, scale
from sklearn.base import TransformerMixin

from PIL import Image
from pathlib import Path
from matplotlib.patches import Circle, Rectangle


def add_colorbar(im, aspect=20, pad_fraction=0.5, **kwargs):
    """Add a vertical color bar to an image plot."""
    divider = axes_grid1.make_axes_locatable(im.axes)
    width = axes_grid1.axes_size.AxesY(im.axes, aspect=1./aspect)
    pad = axes_grid1.axes_size.Fraction(pad_fraction, width)
    current_ax = plt.gca()
    cax = divider.append_axes("right", size=width, pad=pad)
    plt.sca(current_ax)
    return im.axes.figure.colorbar(im, cax=cax, **kwargs)


def scale_img(img_arr):
    """ normalise np array by subtracting its mean and dividing
        by its stdev returning result
    """
    return (img_arr - img_arr.mean())/img_arr.std()


## pipeline for preprocessing images

def preprocess_image(img_arr):
    """
    Create a pipeline to scale pixel values to zero mean and unit stdev
    """
    pipeline = make_pipeline(
        FunctionTransformer(scale_img),
    )
    return pipeline.transform(img_arr)

## pipeline for clustering to derive masks

class BackgroundZeroer(TransformerMixin):
    """ Transformer to send pixel values below a threshold to zero

        Accepts:
            - a 2d N x M numpy array of pixel-wise scalar values
        Returns:
            - a 2d N x M numpy array of pixel-wise scalar values
              where those being < noise_threshold are sent to zero

        noise threshold can be specified manually, or if not,
        it is dervied from a multiplier * the mean array value
    """
    def __init__(self, noise_threshold=None, mean_multiplier=3.):
        self.noise_threshold = noise_threshold
        self.mean_multiplier = mean_multiplier

    def fit(self, X, y=None):
        if self.noise_threshold is None:
            self.noise_threshold = self.mean_multiplier * X.mean()
        return self

    def transform(self, X, y=None):
        _X = X.copy()
        _X[np.where(_X < self.noise_threshold)] = 0
        return _X


class ImageToCoordDataFrame(TransformerMixin):
    """ Transformer to re-express scalar pixel array in a 3d space

        Accepts:
            - X: a 2d N x M numpy array of pixel-wise scalar values
        Returns:
            - an dataframe of (pixel value, x position, y position)
    """
    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        xpx, ypx = X.shape
        indices = np.dstack(np.indices(X.shape))
        coord_form = np.concatenate(
            (X[:,:,np.newaxis], indices), axis=-1)
        _X = pd.DataFrame(data=coord_form.reshape(xpx*ypx, 3),
                          columns=['absorption', 'x', 'y'])
        return _X


class ZeroRemover(TransformerMixin):
    """ Transformer to pick out entries with nonzero absorptions
    """
    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X[X['absorption'] > 0.]


class PositionSelector(TransformerMixin):
    """ Transformer to select position columns for clustering

        Accepts:
            - X: a dataframe with rows x, y and others
        Returns:
            - a dataframe with rows x, y only
    """
    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X[['x', 'y']]


class Clusterer(TransformerMixin):
    """ Transformer wrapper to apply DBSCAN clustering while preserving dataframe structure
    """
    def __init__(self, eps, min_samples):
        self.dbscan = DBSCAN(eps=eps, min_samples=min_samples)

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X.loc[:, 'cluster_label'] = self.dbscan.fit_predict(X.values)
        return X


class MaskExtractor(TransformerMixin):
    """ Transformer to calculate masks (bouding rectangles) for each clustered object

        Accepts:
            - a dataframe with cluster_label, x and y columns
        Returns:
            - a dataframe with ix_xmin, ix_xmax, ix_ymin, ix_ymax columns, one row per clust
    """
    def __init__(self, x_padding=2, y_padding=2):
        """ defaults to adding +/- 2 pixels in each dimension """
        self.x_padding = x_padding
        self.y_padding = y_padding

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """ ignores noise points (labelled -1)"""
        cluster_df = pd.DataFrame()
        for c in X['cluster_label'].unique():
            if c == -1:
                continue
            ixs = X[X['cluster_label'] == c].index
            if len(ixs) <= 1:
                break
            n_points = len(ixs)
            cluster_df.loc[c,'ix_ymin'] = X.loc[ixs,'x'].min().astype(np.int32) - self.y_padding
            cluster_df.loc[c,'ix_ymax'] = X.loc[ixs,'x'].max().astype(np.int32) + self.y_padding
            cluster_df.loc[c,'ix_xmin'] = X.loc[ixs,'y'].min().astype(np.int32) - self.x_padding
            cluster_df.loc[c,'ix_xmax'] = X.loc[ixs,'y'].max().astype(np.int32) + self.x_padding
        return cluster_df


def zero_background(img_arr, noise_threshold=None, mean_multiplier=4.):
    """ send pixels below threshold to zero in numpy array
    """
    if noise_threshold is None:
        noise_threshold = mean_multiplier * img_arr.mean()
    _img_arr = img_arr.copy()
    _img_arr[np.where(_img_arr < noise_threshold)] = 0
    return _img_arr


def extract_masks(img_arr,
                  noise_threshold=None,
                  mean_multiplier=4.,
                  eps=1.,
                  min_samples=2):
    """ Pipeline to extract a dataframe of object masks from original image array

        Accepts:
            - img_arr: a 2d numpy array with scalar pixel values
            - noise_threshold/mean_multiplier: parameters passed to background zeroer
            - eps/min_samples: parameters passed to DBSCAN clustering

        Returns:
            - a dataframe of the rectangular boundaries of each individual object
              detected with DBSCAN clustering
    """
    pipeline = make_pipeline(
        # send pixels below background threshold to 0
        BackgroundZeroer(noise_threshold=noise_threshold,
                         mean_multiplier=mean_multiplier),
        # translate image to 3d
        ImageToCoordDataFrame(),
        # remove background points
        ZeroRemover(),
        # select positions only for clustering
        PositionSelector(),
        # cluster
        Clusterer(eps=eps, min_samples=min_samples),
        # get masks
        MaskExtractor()
    )
    return pipeline.fit_transform(img_arr)


## visualise masks

def visualise_masks(img_data, n_rows_plot=1000):
    """ visualise the masks derived from the clustering step
        on the rescaled image array
    """
    fig, axarr = plt.subplots(1, 3, figsize=(30,20));
    mask_df, im = {}, {}
    ii = 0
    for material, img in img_data.items():
        mask_df[material] = extract_masks(img)
        im[material] = axarr[ii].imshow(img[:n_rows_plot], vmin=0.0, vmax=0.02)
        add_colorbar(im[material])

        for ix, row in mask_df[material].iterrows():
            rec = Rectangle((row['ix_xmin'], row['ix_ymin']),
                             row['ix_xmax'] - row['ix_xmin'],
                             row['ix_ymax'] - row['ix_ymin'],
                             color='#ffffff',
                             fill=True,
                             alpha=0.6)
            axarr[ii].add_patch(rec)
        ii += 1
    plt.show()
